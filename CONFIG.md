# Configuration

Configuration is loaded from a json `repos.json` files from:

1. `$GSINK_CONFIG_PATH`, which can contain multipl `repos.json` paths separated by `:`
1. `$XDG_CONFIG_HOME/repos.json`
1. `$HOME/.config/repos.json`

The `repos.json` can contain a list of repositories, like:

```json
[
  {
    "name": "Repository 1",
    "local": "$HOME/code/repository-1",
    "remote": "https://somegit.com/user/repository-1"
  },
  {
    "name": "Repository 2",
    "local": "$HOME/code/repository-2",
    "remote": "https://somegit.com/user/repository-2"
  }
]
```

Or it can contain global configurations and all the repositories, like:

```json
{
  "settings": {
    "colors": true,
    "afterSync": ["echo", "hi there"]
  },
  "repositories": {
    "Repository 1": {
      "local": "$HOME/code/repository-1",
      "remote": "https://somegit.com/user/repository-1"
    },
    "Repository 2": {
      "local": "$HOME/code/repository-2",
      "remote": "https://somegit.com/user/repository-2"
    }
  }
```

## Available global settings

- `colors`: (`bool`) enable or disable the output colors.
  - default `true`
- `afterSync`: (`[]string`) command to run in the repository after the sync
  - default `[]`
