package main

import (
	"cmp"
	_ "embed"
	"fmt"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"sync"
	"time"

	"gitlab.com/kassio/gsink/internal/configparser"
	"gitlab.com/kassio/gsink/internal/logging"
	"gitlab.com/kassio/gsink/internal/sinker"
)

//go:embed VERSION
var AppVersion string

//go:embed CONFIG.md
var ConfigDoc string

func main() {
	startedAt := time.Now()
	options := parseOptions()
	config := loadConfigFiles(options)

	if options.showConfig {
		showConfig(config)
		os.Exit(0)
	}

	var waitGroup sync.WaitGroup
	waitGroup.Add(len(config.Repositories))

	l := logging.Start(config)

	for _, r := range config.Repositories {
		go func(r configparser.Repository, s configparser.Settings, l logging.Logger, wg *sync.WaitGroup) {
			defer wg.Done()

			sinker.Sync(r, s, l)
		}(r, config.Settings, l, &waitGroup)
	}

	waitGroup.Wait()

	timeElapsed := time.Since(startedAt)
	fmt.Printf("\nTook %s to update\n", timeElapsed.Truncate(time.Second))
}

func loadConfigFiles(options appOptions) configparser.Config {
	configFiles := append(
		options.configFiles,
		defaultConfigFiles()...)

	config := configparser.Parse(configFiles, options.settings)
	slices.SortFunc(config.Repositories, func(a, b configparser.Repository) int {
		return cmp.Compare(a.Name, b.Name)
	})

	return config
}

func defaultConfigFiles() []string {
	paths := os.ExpandEnv("$GSINK_CONFIG_PATH")

	if paths == "" {
		paths = os.ExpandEnv("$XDG_CONFIG_HOME")

		if paths == "" {
			homeDir, _ := os.UserHomeDir()
			paths = filepath.Join(homeDir, ".config")
		}

		paths = filepath.Join(paths, "repos.json")
	}

	return strings.Split(paths, ":")
}

func showConfig(config configparser.Config) {
	for _, repo := range config.Repositories {
		fmt.Printf(`%s:
			Local: %s
			Remote: %s
			`, repo.Name, repo.Local, repo.Remote)
	}

	colors := "disabled"
	if config.Colors {
		colors = "enabled"
	}

	fmt.Println("\nColors:", colors)

	if len(config.AfterSync) > 0 && len(config.AfterSync[0]) > 0 {
		fmt.Println("AfterSync:", strings.Join(config.AfterSync, " "))
	}
}
