package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"strings"
)

type (
	appOptions struct {
		showConfig  bool
		configFiles []string
		settings    map[string]any
	}
)

func parseOptions() appOptions {
	configPaths := ""
	flag.StringVar(
		&configPaths,
		"config",
		configPaths,
		"Path for the configuration files (it accepts multiple paths separated by ':')",
	)

	colorsDisabled := false
	flag.BoolVar(&colorsDisabled, "no-colors", colorsDisabled, "Enable colors (override global settings)")

	afterSync := ""
	flag.StringVar(&afterSync, "after-sync", afterSync, "Command to run on each repository after its synced (override global settings)")

	showConfig := false
	flag.BoolVar(&showConfig, "list", showConfig, "List all configured repositories")

	showVersion := false
	flag.BoolVar(&showVersion, "version", showVersion, "Show version")

	flag.Usage = usage

	flag.Parse()

	if showVersion {
		fmt.Println(AppVersion)
		os.Exit(0)
	}

	settings := map[string]any{
		"override":  true,
		"afterSync": strings.Split(afterSync, " "),
	}

	// Only override colors if the flag is set
	if colorsDisabled {
		settings["colors"] = false
	}

	return appOptions{
		showConfig:  showConfig,
		configFiles: strings.Split(configPaths, ":"),
		settings:    settings,
	}
}

func usage() {
	defaults := new(bytes.Buffer)

	flag.CommandLine.SetOutput(defaults)
	flag.PrintDefaults()
	flag.CommandLine.SetOutput(os.Stdout)

	fmt.Fprintf(
		flag.CommandLine.Output(),
		`Usage: %s [OPTIONS]
OPTIONS:
%s
%s
VERSION: %s`,
		os.Args[0],
		defaults.String(),
		ConfigDoc,
		AppVersion)
}
