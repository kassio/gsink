BUILDER = "github.com/mitchellh/gox@latest"
BUILD_OUTPUT = "bin/gsink-{{.OS}}-{{.Arch}}"
OSARCH = "$(PLATFORM)"
FORMATTER = "github.com/golangci/golangci-lint/cmd/golangci-lint@latest"

.DEFAULT_GOAL := local

.PHONY: all
all: local-format
all: test
all: clean
all: local-build

.PHONY: release
release:
	./release
	git push
	git push --tags

.PHONY: test
test:
	go test -race -v ./...

.PHONY: format
format:
	go run $(FORMATTER) run ./...

format-fix:
	go run $(FORMATTER) run --fix ./...

.PHONY: build
build:
	mkdir -p bin
	go run \
		$(BUILDER) \
		-osarch=$(OSARCH) \
		-output=$(BUILD_OUTPUT)

.PHONY: clean
clean:
	rm -rf bin

.PHONEY: local-dev
local-dev: local-build
local-dev:
	bin/gsink

.PHONY: local-format
local-format:
	golangci-lint run ./...

.PHONY: local-format-fix
local-format-fix:
	golangci-lint run --fix ./...

.PHONY: local-build
local-build:
	go build -o bin/gsink
