# 🚿 gsink

Sync your git repositories made easy

## Motivation

Among work projects, dotfiles, editorconfigs, pet projects, personal projects or any other thing one
might use git to sync it might happen that you have a lot of git repositories with a few branches to keep track of.
Even if you don't share these repositories, you might have bots, pipelines or any other mechanisms
that commits in these repositories.

`gsink` helps you to avoid missing these commits and ensure that your local machine is always in
~~sink~~ sync. (see what I did here? 😬)

## Install

### From releases

Check the [latest](https://gitlab.com/kassio/gsink/-/releases/permalink/latest) release.

### From `go install`

```console
go install "gitlab.com/kassio/gsink@latest"
```

## Usage

Check

```console
gsink --help
```

## [Configuration](CONFIG.md)

## Releases

Releases are managed through GitLab Release build:

### Development releases

- Every commit generates dev binaries (VERSION is the commit sha)
- To build a local release use `make all`

### Stable releases

- Every tag generate a new release ([semver](https://semver.org))
- To create a new release use the `./release [major|minor]` tool, which will
  update the git tag accordingly.
