package configparser

import (
	"encoding/json"
	"fmt"
	"os"
)

func Parse(files []string, settings map[string]any) Config {
	cache := map[string]bool{}
	config := Config{
		Settings: Settings{Colors: true},
	}

	for _, f := range files {
		if f == "" {
			continue
		}

		content, err := os.ReadFile(f)
		if err != nil {
			panic(fmt.Sprintf("failed to read config file: %s", err))
		}

		parseFile(&config, cache, content)
	}

	config.addGlobalSettings(settings)

	return config
}

func parseFile(config *Config, cache map[string]bool, content []byte) {
	var j any

	err := json.Unmarshal(content, &j)
	if err != nil {
		panic(fmt.Sprintf("failed to parse config file: %s", err))
	}

	if repositoriesRaw, ok := j.([]any); ok {
		// When the config file has only the list of repositories
		addRepositoriesFromList(config, cache, repositoriesRaw)
	} else if rawConfig, ok := j.(map[string]any); ok {
		// When the config file has global configs and a list of repositories
		addRepositoriesFromMap(config, cache, rawConfig)

		if rawSettings, ok := rawConfig["settings"].(map[string]any); ok {
			config.addGlobalSettings(rawSettings)
		}
	} else {
		panic(fmt.Sprintf("failed to parse config file (malformed): %s", err))
	}
}

func addRepositoriesFromList(c *Config, cache map[string]bool, repositoriesRaw []any) {
	for _, raw := range repositoriesRaw {
		repo := raw.(map[string]any)
		name := repo["name"].(string)

		c.addRepository(cache, name, repo)
	}
}

func addRepositoriesFromMap(c *Config, cache map[string]bool, rawConfig map[string]any) {
	rawRepositories, ok := rawConfig["repositories"].(map[string]any)

	if !ok {
		panic(`missing "repositories" key`)
	}

	for name, raw := range rawRepositories {
		repo := raw.(map[string]any)

		c.addRepository(cache, name, repo)
	}
}
