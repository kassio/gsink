package configparser_test

import (
	"fmt"
	"os"
	"path"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/kassio/gsink/internal/configparser"
)

type options struct {
	files    []string
	settings map[string]any
}

func TestConfigs(t *testing.T) {
	t.Parallel()

	home := os.Getenv("HOME")

	tests := map[string]struct {
		options      options
		errMsg       string
		parsedConfig configparser.Config
	}{
		"when config file has only a list of repositories": {
			options: options{
				files: []string{fixturePath("only-repositories.json")},
			},
			parsedConfig: configparser.Config{
				Settings: configparser.Settings{
					Colors: true,
				},
				Repositories: []configparser.Repository{
					{
						Name:   "repo name",
						Local:  "/tmp/repo",
						Remote: "https://somegit/user/project1",
					},
					{
						Name:   "repo in home",
						Local:  home + "/repo",
						Remote: "https://somegit/user/project2",
					},
				},
			},
		},
		"when config file has global configs": {
			options: options{
				files: []string{fixturePath("with-global-configs.json")},
			},
			parsedConfig: configparser.Config{
				Settings: configparser.Settings{
					AfterSync: []string{"echo", "hi there"},
					Colors:    false,
				},
				Repositories: []configparser.Repository{
					{
						Name:      "other config repo name",
						Local:     "/tmp/repo",
						Remote:    "https://somegit/user/project1",
						AfterSync: []string{"git", "switch", "main"},
					},
					{
						Name:   "other repo",
						Local:  "/tmp/other-repo",
						Remote: "https://somegit/user/project3",
					},
				},
			},
		},
		"when overriding global setting with an option": {
			options: options{
				files: []string{fixturePath("with-global-configs.json")},
				settings: map[string]any{
					"colors":    true,
					"afterSync": []any{"another", "command"},
				},
			},
			parsedConfig: configparser.Config{
				Settings: configparser.Settings{
					AfterSync: []string{"another", "command"},
					Colors:    true,
				},
				Repositories: []configparser.Repository{
					{
						Name:      "other config repo name",
						Local:     "/tmp/repo",
						Remote:    "https://somegit/user/project1",
						AfterSync: []string{"git", "switch", "main"},
					},
					{
						Name:   "other repo",
						Local:  "/tmp/other-repo",
						Remote: "https://somegit/user/project3",
					},
				},
			},
		},
		"when loading multiple config files": {
			options: options{
				files: []string{
					fixturePath("only-repositories.json"),
					fixturePath("with-global-configs.json"),
				},
			},
			parsedConfig: configparser.Config{
				Settings: configparser.Settings{
					AfterSync: []string{"echo", "hi there"},
					Colors:    false,
				},
				Repositories: []configparser.Repository{
					{
						Name:   "repo name",
						Local:  "/tmp/repo",
						Remote: "https://somegit/user/project1",
					},
					{
						Name:   "repo in home",
						Local:  home + "/repo",
						Remote: "https://somegit/user/project2",
					},
					{
						Name:   "other repo",
						Local:  "/tmp/other-repo",
						Remote: "https://somegit/user/project3",
					},
				},
			},
		},
		"when config file does not exist": {
			options: options{
				files: []string{fixturePath("inexistent")},
			},
			errMsg: fmt.Sprintf(
				"failed to read config file: open %s: no such file or directory",
				fixturePath("inexistent")),
		},
		"when config file is empty": {
			options: options{
				files: []string{fixturePath("empty")},
			},
			errMsg: "failed to parse config file: unexpected end of JSON input",
		},
		"when config file is malformed": {
			options: options{
				files: []string{fixturePath("malformed.json")},
			},
			errMsg: `failed to parse config file: invalid character 'm' looking for beginning of value`,
		},
		"when config file is invalid": {
			options: options{
				files: []string{fixturePath("invalid-configs.json")},
			},
			errMsg: `missing "repositories" key`,
		},
	}

	for name, test := range tests {
		test := test

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			if test.errMsg != "" {
				assert.PanicsWithValue(t, test.errMsg, func() {
					configparser.Parse(test.options.files, test.options.settings)
				})

				return
			}

			config := configparser.Parse(test.options.files, test.options.settings)
			assert.Equal(t, test.parsedConfig.Settings, config.Settings)
			assert.ElementsMatch(t, test.parsedConfig.Repositories, config.Repositories)
		})
	}
}

func fixturePath(name string) string {
	_, f, _, ok := runtime.Caller(0)

	if ok {
		return path.Join(path.Dir(f), "fixtures", name)
	}

	return ""
}
