package configparser

import "os"

type (
	Config struct {
		Repositories []Repository `json:"repositories,omitempty"`
		Settings     `json:"settings,omitempty"`
	}
	Repository struct {
		Local     string   `json:"local,omitempty"`
		Name      string   `json:"name,omitempty"`
		Remote    string   `json:"remote,omitempty"`
		AfterSync []string `json:"afterSync,omitempty"`
	}
	Settings struct {
		Colors    bool     `json:"colors,omitempty"`
		AfterSync []string `json:"afterSync,omitempty"`
	}
)

func (c *Config) addRepository(cache map[string]bool, name string, repo map[string]any) {
	local := repo["local"].(string)
	remote := repo["remote"].(string)

	var afterSync []string
	if list, err := parseAfterSync(repo); err {
		afterSync = list
	}

	if cache[remote] {
		return
	}

	cache[remote] = true

	c.Repositories = append(c.Repositories, Repository{
		Name:      name,
		Local:     os.ExpandEnv(local),
		Remote:    remote,
		AfterSync: afterSync})
}

func (c *Config) addGlobalSettings(settings map[string]any) {
	// AfterSync is always overridden
	if afterSync, ok := parseAfterSync(settings); ok {
		c.AfterSync = afterSync
	}

	if override, ok := settings["override"]; ok && override.(bool) {
		overrideGlobals(c, settings)
	}

	mergeGlobals(c, settings)
}

func overrideGlobals(c *Config, settings map[string]any) {
	if colors, ok := settings["colors"]; ok {
		c.Colors = colors.(bool)
	}
}

func mergeGlobals(c *Config, settings map[string]any) {
	if colors, ok := settings["colors"]; ok {
		c.Colors = colors.(bool)
	}
}

func parseAfterSync(settings map[string]any) ([]string, bool) {
	var args []string

	if afterSyncArgs, ok := settings["afterSync"]; ok {
		switch rawArgs := afterSyncArgs.(type) {
		case ([]any):
			for _, arg := range rawArgs {
				args = append(args, arg.(string))
			}
		case ([]string):
			args = rawArgs
		}
	}

	if len(args) < 1 || len(args[0]) == 0 {
		return []string{}, false
	}

	return args, true
}
