package logging

import "fmt"

type Message struct {
	Name    string
	Branch  string
	Message string
	Color   Color
}

func (m Message) noColor() Message {
	m.Color = None

	return m
}

func (m Message) String() string {
	text := m.Color.c(m.Message)

	if m.Branch == "" {
		return fmt.Sprintf(" %s: %s", m.Name, text)
	}

	return fmt.Sprintf("   %s: %s", m.Branch, text)
}
