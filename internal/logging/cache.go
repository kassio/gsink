package logging

import "strings"

func (logger Logger) cacheAdd(name, branch string, msg Message) {
	logger.cache[key(name, branch)] = msg
}

func (logger Logger) cacheGet(name, branch string) string {
	cached, ok := logger.cache[key(name, branch)]

	if ok {
		return cached.String()
	}

	return Message{
		Name:    name,
		Branch:  branch,
		Color:   logger.defaultColor(),
		Message: "waiting",
	}.String()
}

func (logger Logger) defaultColor() Color {
	if logger.colorsEnabled {
		return Yellow
	}

	return None
}

func key(name, branch string) string {
	return strings.Join([]string{name, branch}, ".")
}
