package logging

import "fmt"

type Color int

const (
	None   Color = -1
	Red    Color = 31
	Green  Color = 32
	Yellow Color = 33
	Blue   Color = 34
)

func (c Color) c(msg string) string {
	if c == None {
		return msg
	}

	return fmt.Sprintf("\033[%dm%s\033[0m", c, msg)
}
