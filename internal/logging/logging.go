package logging

import (
	"fmt"
	"strings"

	"github.com/gosuri/uilive"

	"gitlab.com/kassio/gsink/internal/configparser"
	"gitlab.com/kassio/gsink/internal/git"
)

type Logger struct {
	colorsEnabled bool
	cache         map[string]Message
	c             chan Message
}

func (logger Logger) Log(msg Message) {
	if !logger.colorsEnabled {
		logger.c <- msg.noColor()
		return
	}

	logger.c <- msg
}

func Start(config configparser.Config) Logger {
	logger := Logger{
		c:             make(chan Message),
		cache:         make(map[string]Message),
		colorsEnabled: config.Colors,
	}

	repos := []git.Repository{}
	for _, repositoryConfig := range config.Repositories {
		repos = append(repos, git.Repository{
			Name:   repositoryConfig.Name,
			Local:  repositoryConfig.Local,
			Remote: repositoryConfig.Remote,
		})
	}

	go printOutput(logger, repos)

	return logger
}

func printOutput(logger Logger, repositories []git.Repository) {
	writer := uilive.New()
	writer.Start()

	for message := range logger.c {
		logger.cacheAdd(message.Name, message.Branch, message)
		outputLines := []string{}

		for _, repository := range repositories {
			outputLines = append(outputLines, logger.cacheGet(repository.Name, ""))

			branchMain := repository.BranchMain()
			outputLines = append(outputLines, logger.cacheGet(repository.Name, branchMain))

			branches, _ := repository.GetBranches()

			for _, branch := range branches {
				if branch == branchMain {
					continue
				}

				outputLines = append(outputLines, logger.cacheGet(repository.Name, branch))
			}
		}
		fmt.Fprintln(writer, strings.Join(outputLines, "\n"))
	}
	writer.Stop()
}
