package git_test

import (
	"os/exec"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/kassio/gsink/internal/git"
)

type fakeExecuter struct {
	called bool
	cmd    *exec.Cmd
}

func (f *fakeExecuter) Output(cmd *exec.Cmd) (string, error) {
	f.called = true
	f.cmd = cmd

	return "", nil
}

func TestGit(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		fn  func(git.Repository) (string, error)
		err error
		cmd []string
	}{
		"clone": {
			cmd: []string{"git", "clone", "remote", "/tmp/fake-test"},
			fn: func(repo git.Repository) (string, error) {
				return "", repo.Clone()
			},
		},
		"fetch": {
			cmd: []string{"git", "fetch", "--all"},
			fn: func(repo git.Repository) (string, error) {
				return "", repo.Fetch()
			},
		},
		"rebase": {
			cmd: []string{"git", "rebase", "remote/branch", "branch"},
			fn: func(repo git.Repository) (string, error) {
				return "", repo.Rebase("remote", "branch")
			},
		},
		"branch": {
			cmd: []string{"git", "symbolic-ref", "--short", "HEAD"},
			fn: func(repo git.Repository) (string, error) {
				return repo.Branch(), nil
			},
		},
		"get branchs": {
			cmd: []string{"git", "for-each-ref", "--format='%(refname:short)'", "refs/heads"},
			fn: func(repo git.Repository) (string, error) {
				branches, err := repo.GetBranches()
				return branches[0], err
			},
		},
		"branch remote": {
			cmd: []string{"git", "config", "--get", "branch.branch.remote"},
			fn: func(repo git.Repository) (string, error) {
				return repo.BranchRemote("branch"), nil
			},
		},
		"branch main": {
			cmd: []string{"git", "symbolic-ref", "refs/remotes/origin/HEAD", "--short"},
			fn: func(repo git.Repository) (string, error) {
				return repo.BranchMain(), nil
			},
		},
	}

	for name, test := range tests {
		test := test

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			executer := &fakeExecuter{}
			repo := git.Repository{
				Name:     "name",
				Local:    "/tmp/fake-test",
				Remote:   "remote",
				Executer: executer,
			}

			_, err := test.fn(repo)

			assert.True(t, executer.called)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.cmd, executer.cmd.Args)
		})
	}
}
