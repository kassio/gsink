package git

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"
)

type (
	executer interface {
		Output(*exec.Cmd) (string, error)
	}

	Repository struct {
		Name      string
		Local     string
		Remote    string
		Branches  []string
		Executer  executer
		AfterSync []string
	}
)

func (r Repository) Clone() error {
	_, err := r.Output(exec.Command("git", "clone", r.Remote, r.Local)) //nolint:gosec

	return err
}

func (r Repository) Fetch() error {
	if _, err := execLocal(r, "fetch", "--all"); err != nil {
		return err
	}

	return nil
}

func (r Repository) Rebase(remote, branch string) error {
	if _, err := execLocal(r, "rebase", fmt.Sprintf("%s/%s", remote, branch), branch); err != nil {
		return err
	}

	return nil
}

func (r Repository) Branch() string {
	out, err := execLocal(r, "symbolic-ref", "--short", "HEAD")
	if err != nil {
		return ""
	}

	return strings.TrimSpace(out)
}

func (r Repository) BranchRemote(branch string) string {
	out, err := execLocal(r, "config", "--get", fmt.Sprintf("branch.%s.remote", branch))
	if err != nil {
		return ""
	}

	return strings.TrimSpace(out)
}

func (r Repository) BranchMain() string {
	ref, err := execLocal(r, "symbolic-ref", "refs/remotes/origin/HEAD", "--short")
	if err != nil || ref == "" {
		return "master"
	}

	branch := strings.Split(ref, "/")[1]
	return strings.TrimSpace(branch)
}

func (r *Repository) GetBranches() ([]string, error) {
	if len(r.Branches) > 0 {
		return r.Branches, nil
	}

	list, err := execLocal(*r, "for-each-ref", "--format='%(refname:short)'", "refs/heads")
	if err != nil {
		return []string{}, err
	}

	list = strings.ReplaceAll(list, "'", "")
	list = strings.TrimSpace(list)

	r.Branches = strings.Split(list, "\n")

	return r.Branches, nil
}

func execLocal(r Repository, args ...string) (string, error) {
	cmd := exec.Command("git", args...)
	cmd.Dir = r.Local

	return r.Output(cmd)
}

func (r Repository) Output(cmd *exec.Cmd) (string, error) {
	var output string
	var err error

	if r.Executer != nil {
		output, err = r.Executer.Output(cmd)
	} else {
		var bytes []byte
		bytes, err = cmd.Output()
		output = string(bytes)
	}

	if err != nil {
		errmsg := fmt.Errorf("`%s`: %w", strings.Join(cmd.Args, " "), err)
		logToFile(r, cmd, "/tmp/gsink.log", errmsg.Error())

		return "", errmsg
	}

	logToFile(r, cmd, "/tmp/gsink.log", output)
	return output, nil
}

func logToFile(r Repository, cmd *exec.Cmd, filename, output string) {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	text := fmt.Sprintf(
		"[%s]%s %s > %s\n",
		time.Now().Format("20060102150405"),
		r.Local,
		cmd.Args,
		strings.ReplaceAll(strings.TrimSpace(output), "\n", ", "))

	if _, err = f.WriteString(text); err != nil {
		panic(err)
	}
}
