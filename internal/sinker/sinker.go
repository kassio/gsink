package sinker

import (
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/kassio/gsink/internal/configparser"
	"gitlab.com/kassio/gsink/internal/git"
	"gitlab.com/kassio/gsink/internal/logging"
)

type repo struct {
	git.Repository
	logger logging.Logger
}

func Sync(repository configparser.Repository, settings configparser.Settings, logger logging.Logger) {
	afterSync := settings.AfterSync
	if len(repository.AfterSync) > 0 {
		afterSync = repository.AfterSync
	}

	r := repo{
		Repository: git.Repository{
			Name:      repository.Name,
			Local:     repository.Local,
			Remote:    repository.Remote,
			AfterSync: afterSync,
		},
		logger: logger,
	}

	r.green("syncing")

	if err := r.sync(); err != nil {
		r.red(fmt.Errorf("failed"))
	}

	if err := r.runAfterSync(); err != nil {
		r.red(err)
		return
	}

	r.blue("synced")
}

func (r repo) sync() error {
	if _, err := os.Stat(r.Local); err != nil && os.IsNotExist(err) {
		return r.clone()
	}

	return r.update()
}

func (r repo) clone() error {
	r.green("clonning")

	if err := r.Clone(); err != nil {
		return err
	}

	r.blue("cloned")
	return nil
}

func (r repo) update() error {
	r.green("updating")

	mainBranch := r.BranchMain()

	branches, err := r.GetBranches()
	if err != nil {
		return fmt.Errorf("fetch branches: %w", err)
	}

	r.green("fetching remotes")
	if err = r.Fetch(); err != nil {
		return fmt.Errorf("fetch: %w", err)
	}

	r.green("updating branches")
	r.updateBranch(mainBranch)

	for _, branch := range branches {
		if branch == mainBranch {
			continue
		}

		r.updateBranch(branch)
	}

	r.blue("updated")
	return nil
}

func (r repo) updateBranch(branch string) {
	remote := r.BranchRemote(branch)
	if remote == "" {
		r.redBranch(branch, fmt.Errorf("no remote"))
		return
	}

	r.greenBranch(branch, "updating")

	if err := r.Rebase(remote, branch); err != nil {
		r.redBranch(branch, fmt.Errorf("fail to rebase, you might have unstaged changes"))
		return
	}

	r.blueBranch(branch, "updated")
}

func (r repo) runAfterSync() error {
	if len(r.AfterSync) <= 0 || len(r.AfterSync[0]) == 0 {
		return nil
	}

	r.green("%s", r.AfterSync)

	cmd := exec.Command(r.AfterSync[0], r.AfterSync[1:]...) //nolint:gosec
	cmd.Env = append(cmd.Environ(), "PATH="+os.ExpandEnv("$PATH"))
	cmd.Dir = r.Local

	out, err := cmd.Output()
	if err != nil {
		return fmt.Errorf("[after sync] `%s`: %w", r.AfterSync, err)
	}

	r.green(string(out))
	return nil
}

func (r repo) green(format string, args ...any) {
	r.log(logging.Green, format, args...)
}

func (r repo) blue(format string, args ...any) {
	r.log(logging.Blue, format, args...)
}

func (r repo) red(err error) {
	r.log(logging.Red, err.Error())
}

func (r repo) greenBranch(branch, format string, args ...any) {
	r.logBranch(logging.Green, branch, format, args...)
}

func (r repo) blueBranch(branch string, format string, args ...any) {
	r.logBranch(logging.Blue, branch, format, args...)
}

func (r repo) redBranch(branch string, err error) {
	r.logBranch(logging.Red, branch, err.Error())
}

func (r repo) log(color logging.Color, format string, args ...any) {
	r.logger.Log(logging.Message{
		Color:   color,
		Name:    r.Name,
		Message: fmt.Sprintf(format, args...),
	})
}

func (r repo) logBranch(color logging.Color, branch, format string, args ...any) {
	r.logger.Log(logging.Message{
		Color:   color,
		Name:    r.Name,
		Branch:  branch,
		Message: fmt.Sprintf(format, args...),
	})
}
